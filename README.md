# Declaration of Datum Property Rights [DRAFT]

We are organizing a distributed convention to get consensus on Datum Property Rights for the New Web.

## Sections
1. Preamble
2. Define data
3. Delineate individual and collective ownership and stewardship 

## Open Participation
We are all free to participate if we follow these rules. 
1. Come with an open mind
2. Honor your word with complete integrity
3. Communication without accusations and disrespect
4. Seek universally fair solutions
5. Take responsiblilty for raising cohumanity - people and planet

## Participation Guide
