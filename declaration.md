# Declaration of Datum Property Rights 0.1 [DRAFT]

## Preamble

We, the people, have collectively created the most powerful and productive invention for knowledge, collabration, and communication called the Web. Unfortunatley, it has become extremely fragmented and controlled. We aim to mend the Web by defining datum property rights, establishing tech standards, and inviting global cooperation to reform the Web on a solid foundation of data unions.


## Impitus

The Cloud Crisis is the realization of mass data property abuse violating an individual's liberty rights in property, choice, and privacy and civil rights in fair elections, free press, and free enterprise resulting in the global "Own Your Data" movement.


## Understanding Data

Withing the field of computing, data is recorded information, and a device contains two primary types of data: 

1. routine-data or dataro - the coded instructions for how the device will operate (e.g., software, apps, OS, environment) or the design or app layer of a website
2. datus - the contents of the software that makes the device's operations relevant to a task or personal to the operators (e.g., settings, documents, files, photos, logs, inputs) or the contents of a website

Within the tech industry, data most often refers to datus which is either collected or user-inputed and may include additional derived or processed data called post-data. Datus must further be distinctivized into:

1. datum - personal, semantic data or a block of data sourced from an individual, which includes data that describes or identifies the originator's actions, associations, or properties
2. codatum - not a set of datum, but a block of data sourced from a group of individuals collaboration or group activity
3. teledatum - any datus that is not datum nor codatum in that no group, association, or individual can be identified or described, with a significant degree of certainity, e.g., privacious (privacy-respecting) telemetry, differentially private post-data, or impersonal data


## Ownership and Stewardship

### Stakeholders

- Owner - datum owner or codatum owners
- Governate - data union or delegate to govern one's datum
- Custodian - a host of the data

### Individual Ownership



### Collective Ownership



### Stewardship

